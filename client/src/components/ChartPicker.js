import React, {Component} from 'react';
import ChartPickerPanel from '../components/ChartPickerPanel';
import Charts from '../components/Charts';

class ChartPicker extends Component{
  constructor(props){
    super(props);
    this.state = {
      userChordCharts: ['Blues Chord', 'Jazz Chord Chart', 'Sad Sounding Chords Chart'],
      chordChartTitle: "Choose a guitar chart"
    };
    this.handleChartChange = this.handleChartChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChartChange(event){
    this.setState({chordChartTitle: event.target.value});
  }

  handleSubmit(event){
    event.preventDefault();
    this.setState({chosenChart: this.state.chordChartTitle});
  }

  render(){
    return(
      <React.Fragment>
        <ChartPickerPanel chordChart={this.state.chordChartTitle} chartChange={this.handleChartChange} submit={this.handleSubmit}/>
      </React.Fragment>
    );
  }
}

export default ChartPicker;
