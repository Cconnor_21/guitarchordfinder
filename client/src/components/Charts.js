import React, {Component} from 'react';
import {Jumbotron, Button} from 'reactstrap';
import ChartBox from '../components/ChartBox';


class Charts extends Component{
  constructor(props){
    super(props);
    //hard coded chord values for rendering charts
    var a1 = {
      id: '1',
      array: [0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
      title: 'A'
    }

    var am1 = {
      id: '2',
      array: [0,0,0,0,1,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
      title: 'Am'
    }

    var am2 = {
      id: '3',
      array: [0,0,0,0,0,0,0,0,1,1,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0],
      title: 'Am'
    }

    var am3 = {
      id: '4',
      array: [0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0],
      title: 'Am'
    }

    var asus1 = {
      id: '5',
      array: [0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0],
      title: 'Asus'
    }

    var aChordChart = [a1];
    var amChordChart = [am1, am2, am3];
    var asusChordChart = [asus1];

    this.state = {
      a1: a1,
      am1: am1,
      am2: am2,
      am3: am3,
      aChordChart: aChordChart,
      amChordChart: amChordChart,
      asusChordChart: asusChordChart

    };

  }

  componentDidMount() {
    console.log('Charts Component did mount');
  }

  componentWillUnmount() {
    console.log('Charts Component will unmount');
  }


  render(){

    var jumbotronStyle = {
      marginBottom: '0',
      backgroundColor: '#494949',
      borderRadius: '0px',
      padding: '25px'
    };


    var chartContent;
    if(this.props.chosenChord === 'Am'){
      chartContent = this.state.amChordChart.map((chord) => <ChartBox  key={chord.id}  chordName={chord.title} chord={chord.array} /> );
    }
    else if(this.props.chosenChord === 'Amaj')
    {
      chartContent = this.state.aChordChart.map((chord) => <ChartBox  key={chord.id}  chordName={chord.title} chord={chord.array} /> );
    }
    else if(this.props.chosenChord === 'Asus')
    {
      chartContent = this.state.asusChordChart.map((chord) => <ChartBox  key={chord.id}  chordName={chord.title} chord={chord.array} /> );
    }

    return (
      <div>
        <Jumbotron style={jumbotronStyle}>
          <div  id="chartWrap">
          {chartContent}
          </div>
        </Jumbotron>
      </div>
    );
  }
}

export default Charts;
