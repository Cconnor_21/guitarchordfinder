import React, {Component} from 'react';
import { Button } from 'reactstrap';
import ChartModal from '../components/ChartModal';
import Chart from '../components/Chart';
import '../../node_modules/animate.css/animate.css';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import ReactHtmlParser from 'react-html-parser';
import '../../node_modules/font-awesome/css/font-awesome.min.css';
var parse = require('html-react-parser');

class ChartBox extends Component{
  constructor(props){
    super(props);
    this.state = {
      viewButtonLabel: 'view',
      saveButtonLabel: 'save',
      currentNote: '',
      toolTipOpacity: '0',
    };
    this.handleClick = this.handleClick.bind(this);
    this.onHover = this.onHover.bind(this);
    this.afterHover = this.afterHover.bind(this);
  }

  onHover(e){
    var note = e.target.id.split('-');
    this.setState({currentNote: note[1], toolTipOpacity: 1}, function(){
    });
  }

  afterHover(){
    this.setState({currentNote: '', toolTipOpacity: 0}, function(){
    });
  }


  handleClick(event){
    console.log("you clicked the item");
  }

  componentDidMount() {
    console.log('ChartBox Component did mount');
  }

  componentWillUnmount() {
    console.log('ChartBox Component will unmount');
  }

  render(){
    var buttonStyle = {
      margin:'3px',
      fontSize: '15px'
    }

    var buttonGroup = {
      marginTop:'10px',
      display: 'block'
    }

    var tooltip = {
      background: 'transparent',
      color: 'black',
      padding: '5px',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      opacity: this.state.toolTipOpacity,
      position: 'absolute',
      marginLeft:'0px',
      marginTop:'0px'
    }

    var tooltipText = {
      background: 'white',
      padding:'6px',
      width:'40px',
      marginLeft: '125px',
      borderRadius: '10px',
    }

    return(
      <React.Fragment>
        <div className="graphBox animated zoomIn">
          <div style={tooltip}><div style={tooltipText}>{this.state.currentNote}</div></div>
          <h2>{this.props.chordName}</h2>
          <Chart chartclass={'chart'} onhover={this.onHover} afterhover={this.afterHover} circleclass={'circle'} chartName={this.props.chord}/>
          <div style={buttonGroup}>
            <Button style={buttonStyle} color="success">
              <i className="fa fa-play"></i>
            </Button>
            <ChartModal btnStyle={buttonStyle} chartclass={'modalchart'} circleclass={'modalcircle'} chartName={this.props.chord} buttonStyle={'primary'} label={this.state.viewButtonLabel}/>
            <ChartModal btnStyle={buttonStyle} buttonStyle={'secondary'} label={this.state.saveButtonLabel}/>
          </div>
        </div>
      </React.Fragment>
    );
  }
}


export default ChartBox;
