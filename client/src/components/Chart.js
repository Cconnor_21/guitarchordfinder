import React, {Component} from 'react';

class Chart extends Component{
  constructor(props){
    super(props);
    this.state = {
    };
    this.onHover = this.onHover.bind(this);
    this.emptyFunction = this.emptyFunction.bind(this);
  }

  onHover(e){
  }

  emptyFunction(){

  }

  render(){
    var chartName = this.props.chartName;

    var circleStyling = {
        opacity:'1',
        fillOpacity:'1',
        strokeWidth:'.52999997',
        strokeMiterlimit:'4',
        strokeDasharray:'none',
        strokeOpacity:'1'
    }

    var pathStyling = {
      fontStyle:'normal',
      fontVariant:'normal',
      fontWeight:'normal',
      fontStretch:'normal',
      fontSize:'medium',
      lineHeight:'normal',
      fontFamily:'sans-serif',
      fontVariantLigatures:'normal',
      fontVariantPosition:'normal',
      fontVariantCaps:'normal',
      fontVariantNumeric:'normal',
      fontVariantAlternates:'normal',
      fontFeatureSettings:'normal',
      textIndent:'0',
      textAlign:'start',
      textDecoration:'none',
      textDecorationLine:'none',
      textDecorationStyle:'solid',
      textDecorationColor:'#000000',
      letterSpacing:'normal',
      wordSpacing:'normal',
      textTransform:'none',
      writingMode:'lr-tb',
      direction:'ltr',
      textOrientation:'mixed',
      dominantBaseline:'auto',
      baselineShift:'baseline',
      textAnchor:'start',
      whiteSpace:'normal',
      shapePadding:'0',
      clipRule:'nonzero',
      display:'inline',
      overflow:'visible',
      visibility:'visible',
      opacity:'1',
      isolation:'auto',
      mixBlendMode:'normal',
      colorInterpolation:'sRGB',
      colorInterpolationFilters:'linearRGB',
      solidColor:'#000000',
      solidOpacity:'1',
      vectorEffect:'none',
      fillOpacity:'1',
      fillRule:'nonzero',
      stroke:'none',
      strokeWidth:'1.854442',
      strokeLinecap:'butt',
      strokeLinejoin:'miter',
      strokeMiterLimit:'4',
      strokeDasharray:'none',
      strokeDashoffset:'0',
      strokeOpacity:'1',
      colorRendering:'auto',
      imageRendering:'auto',
      shapeRendering:'auto',
      textRendering:'auto',
      enableBackground:'accumulate'
    }

    var circleClass = this.props.circleclass;
    var chartClass = this.props.chartclass;

    return(
      <React.Fragment>
      <svg
         xmlns="http://www.w3.org/2000/svg"
         id="svg1351"
         version="1.1"
         viewBox="0 0 122.70254 129.2305"
         height="50mm">
        <defs
           id="defs1345" />
        <g
           transform="translate(-45.981721,-85.076829)"
           id="layer1">
          <path
            className={chartClass}
             id="path1051"
             d="m 56.092511,91.376563 v 3.138083 h -0.02009 V 209.13866 h 0.02716 v 0.15681 H 159.42351 v -1.86439 -103.25304 h 0.004 V 91.376563 Z m 1.833585,12.801477 h 18.701749 v 18.09747 H 57.926096 Z m 20.554675,0 h 18.382423 v 18.09747 H 78.480771 Z m 20.236065,0 h 18.381704 v 18.09747 H 98.716836 Z m 20.235344,0 h 18.38171 v 18.09747 h -18.38171 z m 20.23534,0 h 18.37957 v 18.09747 h -18.37957 z m -81.261424,19.95111 h 18.701749 v 19.49146 H 57.926096 Z m 20.554675,0 h 18.382423 v 19.49146 H 78.480771 Z m 20.236065,0 h 18.381704 v 19.49146 H 98.716836 Z m 20.235344,0 h 18.38171 v 19.49146 h -18.38171 z m 20.23534,0 h 18.37957 v 19.49146 h -18.37957 z m -81.261424,21.3451 h 18.701749 v 19.49146 H 57.926096 Z m 20.554675,0 h 18.382423 v 19.49146 H 78.480771 Z m 20.236065,0 h 18.381704 v 19.49146 H 98.716836 Z m 20.235344,0 h 18.38171 v 19.49146 h -18.38171 z m 20.23534,0 h 18.37957 v 19.49146 H 139.18752 Z M 57.926096,166.81934 H 76.627845 V 186.3108 H 57.926096 Z m 20.554675,0 H 96.863194 V 186.3108 H 78.480771 Z m 20.236065,0 H 117.09854 V 186.3108 H 98.716836 Z m 20.235344,0 h 18.38171 v 19.49146 h -18.38171 z m 20.23534,0 h 18.37957 v 19.49146 h -18.37957 z m -81.261424,21.34509 h 18.701749 v 19.26665 H 57.926096 Z m 20.554675,0 h 18.382423 v 19.26665 H 78.480771 Z m 20.236065,0 h 18.381704 v 19.26665 H 98.716836 Z m 20.235344,0 h 18.38171 v 19.26665 h -18.38171 z m 20.23534,0 h 18.37957 v 19.26665 h -18.37957 z"
             style={pathStyling} />
          <circle
            style={{opacity:''+ chartName[0] +'',
            fillOpacity:'1',
            strokeWidth:'.52999997',
            strokeMiterlimit:'4',
            strokeDasharray:'none',
            strokeOpacity:'1',
            cursor: ''+ (chartName[0] === 1 ? 'pointer' : 'default') +''
          }}

            className={circleClass}
            onMouseOver={chartName[0] === 1 ? this.props.onhover : this.emptyFunction}
            onMouseOut={chartName[0] === 1 ? this.props.afterhover : this.emptyFunction}
             r="6.1739259"
             cy="112.9448"
             cx="57.007862"
             id="0-F" />
          <circle
            style={{opacity:''+ chartName[1] +'',
            fillOpacity:'1',
            strokeWidth:'.52999997',
            strokeMiterlimit:'4',
            strokeDasharray:'none',
            strokeOpacity:'1',
            cursor: ''+ (chartName[1] === 1 ? 'pointer' : 'default') +''
          }}
             className={circleClass}
             onMouseOver={chartName[1] === 1 ? this.props.onhover : this.emptyFunction}
             onMouseOut={chartName[1] === 1 ? this.props.afterhover : this.emptyFunction}
             id="1-A#"
             cx="77.400528"
             cy="112.9448"
             r="6.1739259" />
          <circle
          style={{opacity:''+ chartName[2] +'',
          fillOpacity:'1',
          strokeWidth:'.52999997',
          strokeMiterlimit:'4',
          strokeDasharray:'none',
          strokeOpacity:'1',
          cursor: ''+ (chartName[2] === 1 ? 'pointer' : 'default') +''
          }}
             className={circleClass}
             onMouseOver={chartName[2] === 1 ? this.props.onhover : this.emptyFunction}
             onMouseOut={chartName[2] === 1 ? this.props.afterhover : this.emptyFunction}
             r="6.1739259"
             cy="112.9448"
             cx="97.773445"
             id="2-D#" />
          <circle
          style={{opacity:''+ chartName[3] +'',
          fillOpacity:'1',
          strokeWidth:'.52999997',
          strokeMiterlimit:'4',
          strokeDasharray:'none',
          strokeOpacity:'1',
          cursor: ''+ (chartName[3] === 1 ? 'pointer' : 'default') +''
        }}
             className={circleClass}
             onMouseOver={chartName[3] === 1 ? this.props.onhover : this.emptyFunction}
             onMouseOut={chartName[3] === 1 ? this.props.afterhover : this.emptyFunction}
             id="3-G#"
             cx="118.14637"
             cy="112.9448"
             r="6.1739259" />
          <circle
          style={{opacity:''+ chartName[4] +'',
          fillOpacity:'1',
          strokeWidth:'.52999997',
          strokeMiterlimit:'4',
          strokeDasharray:'none',
          strokeOpacity:'1',
          cursor: ''+ (chartName[4] === 1 ? 'pointer' : 'default') +''
        }}
            onMouseOver={chartName[4] === 1 ? this.props.onhover : this.emptyFunction}
            onMouseOut={chartName[4] === 1 ? this.props.afterhover : this.emptyFunction}
             className={circleClass}
             r="6.1739259"
             cy="112.9448"
             cx="138.51927"
             id="4-C"/>
          <circle
          style={{opacity:''+ chartName[5] +'',
          fillOpacity:'1',
          strokeWidth:'.52999997',
          strokeMiterlimit:'4',
          strokeDasharray:'none',
          strokeOpacity:'1',
          cursor: ''+ (chartName[5] === 1 ? 'pointer' : 'default') +''
        }}
             className={circleClass}
             onMouseOver={chartName[5] === 1 ? this.props.onhover : this.emptyFunction}
             onMouseOut={chartName[5] === 1 ? this.props.afterhover : this.emptyFunction}
             id="5-F"
             cx="158.62761"
             cy="112.9448"
             r="6.1739259" />
          <circle
          style={{opacity:''+ chartName[6] +'',
          fillOpacity:'1',
          strokeWidth:'.52999997',
          strokeMiterlimit:'4',
          strokeDasharray:'none',
          strokeOpacity:'1',
          cursor: ''+ (chartName[6] === 1 ? 'pointer' : 'default') +''
        }}
             className={circleClass}
             onMouseOver={chartName[6] === 1 ? this.props.onhover : this.emptyFunction}
             onMouseOut={chartName[6] === 1 ? this.props.afterhover : this.emptyFunction}
             id="6-F#"
             cx="57.007862"
             cy="134.37605"
             r="6.1739259" />
          <circle
          style={{opacity:''+ chartName[7] +'',
          fillOpacity:'1',
          strokeWidth:'.52999997',
          strokeMiterlimit:'4',
          strokeDasharray:'none',
          strokeOpacity:'1',
          cursor: ''+ (chartName[7] === 1 ? 'pointer' : 'default') +''
        }}
             className={circleClass}
             onMouseOver={chartName[7] === 1 ? this.props.onhover : this.emptyFunction}
             onMouseOut={chartName[7] === 1 ? this.props.afterhover : this.emptyFunction}
             r="6.1739259"
             cy="134.37605"
             cx="77.400528"
             id="7-B"/>
          <circle
          style={{opacity:''+ chartName[8] +'',
          fillOpacity:'1',
          strokeWidth:'.52999997',
          strokeMiterlimit:'4',
          strokeDasharray:'none',
          strokeOpacity:'1',
          cursor: ''+ (chartName[8] === 1 ? 'pointer' : 'default') +''
        }}
             className={circleClass}
             onMouseOver={chartName[8] === 1 ? this.props.onhover : this.emptyFunction}
             onMouseOut={chartName[8] === 1 ? this.props.afterhover : this.emptyFunction}
             id="8-E"
             cx="97.773445"
             cy="134.37605"
             r="6.1739259" />
          <circle
          style={{opacity:''+ chartName[9] +'',
          fillOpacity:'1',
          strokeWidth:'.52999997',
          strokeMiterlimit:'4',
          strokeDasharray:'none',
          strokeOpacity:'1',
          cursor: ''+ (chartName[9] === 1 ? 'pointer' : 'default') +''
        }}
             className={circleClass}
             onMouseOver={chartName[9] === 1 ? this.props.onhover : this.emptyFunction}
             onMouseOut={chartName[9] === 1 ? this.props.afterhover : this.emptyFunction}
             r="6.1739259"
             cy="134.37605"
             cx="118.14637"
             id="9-A"/>
          <circle
          style={{opacity:''+ chartName[10] +'',
          fillOpacity:'1',
          strokeWidth:'.52999997',
          strokeMiterlimit:'4',
          strokeDasharray:'none',
          strokeOpacity:'1',
          cursor: ''+ (chartName[10] === 1 ? 'pointer' : 'default') +''
        }}
             className={circleClass}
             onMouseOver={chartName[10] === 1 ? this.props.onhover : this.emptyFunction}
             onMouseOut={chartName[10] === 1 ? this.props.afterhover : this.emptyFunction}
             id="10-C#"
             cx="138.51927"
             cy="134.37605"
             r="6.1739259" />
          <circle
          style={{opacity:''+ chartName[11] +'',
          fillOpacity:'1',
          strokeWidth:'.52999997',
          strokeMiterlimit:'4',
          strokeDasharray:'none',
          strokeOpacity:'1',
          cursor: ''+ (chartName[11] === 1 ? 'pointer' : 'default') +''
        }}
             className={circleClass}
             onMouseOver={chartName[11] === 1 ? this.props.onhover : this.emptyFunction}
             onMouseOut={chartName[11] === 1 ? this.props.afterhover : this.emptyFunction}
             r="6.1739259"
             cy="134.37605"
             cx="158.62761"
             id="11-F#"/>
          <circle
          style={{opacity:''+ chartName[12] +'',
          fillOpacity:'1',
          strokeWidth:'.52999997',
          strokeMiterlimit:'4',
          strokeDasharray:'none',
          strokeOpacity:'1',
          cursor: ''+ (chartName[12] === 1 ? 'pointer' : 'default') +''
        }}
             className={circleClass}
             onMouseOver={chartName[12] === 1 ? this.props.onhover : this.emptyFunction}
             onMouseOut={chartName[12] === 1 ? this.props.afterhover : this.emptyFunction}
             r="6.1739259"
             cy="155.54271"
             cx="57.007862"
             id="12-G"/>
          <circle
          style={{opacity:''+ chartName[13] +'',
          fillOpacity:'1',
          strokeWidth:'.52999997',
          strokeMiterlimit:'4',
          strokeDasharray:'none',
          strokeOpacity:'1',
          cursor: ''+ (chartName[13] === 1 ? 'pointer' : 'default') +''
        }}
             className={circleClass}
             onMouseOver={chartName[13] === 1 ? this.props.onhover : this.emptyFunction}
             onMouseOut={chartName[13] === 1 ? this.props.afterhover : this.emptyFunction}
             id="13-C"
             cx="77.400528"
             cy="155.54271"
             r="6.1739259" />
          <circle
          style={{opacity:''+ chartName[14] +'',
          fillOpacity:'1',
          strokeWidth:'.52999997',
          strokeMiterlimit:'4',
          strokeDasharray:'none',
          strokeOpacity:'1',
          cursor: ''+ (chartName[14] === 1 ? 'pointer' : 'default') +''
        }}
             className={circleClass}
             onMouseOver={chartName[14] === 1 ? this.props.onhover : this.emptyFunction}
             onMouseOut={chartName[14] === 1 ? this.props.afterhover : this.emptyFunction}
             r="6.1739259"
             cy="155.54271"
             cx="97.773445"
             id="14-F"/>
          <circle
          style={{opacity:''+ chartName[15] +'',
          fillOpacity:'1',
          strokeWidth:'.52999997',
          strokeMiterlimit:'4',
          strokeDasharray:'none',
          strokeOpacity:'1',
          cursor: ''+ (chartName[15] === 1 ? 'pointer' : 'default') +''
        }}
             className={circleClass}
             onMouseOver={chartName[15] === 1 ? this.props.onhover : this.emptyFunction}
             onMouseOut={chartName[15] === 1 ? this.props.afterhover : this.emptyFunction}
             id="15-A#"
             cx="118.14637"
             cy="155.54271"
             r="6.1739259" />
          <circle
          style={{opacity:''+ chartName[16] +'',
          fillOpacity:'1',
          strokeWidth:'.52999997',
          strokeMiterlimit:'4',
          strokeDasharray:'none',
          strokeOpacity:'1',
          cursor: ''+ (chartName[16] === 1 ? 'pointer' : 'default') +''
        }}
             className={circleClass}
             onMouseOver={chartName[16] === 1 ? this.props.onhover : this.emptyFunction}
             onMouseOut={chartName[16] === 1 ? this.props.afterhover : this.emptyFunction}
             r="6.1739259"
             cy="155.54271"
             cx="138.51927"
             id="16-D"/>
          <circle
          style={{opacity:''+ chartName[17] +'',
          fillOpacity:'1',
          strokeWidth:'.52999997',
          strokeMiterlimit:'4',
          strokeDasharray:'none',
          strokeOpacity:'1',
          cursor: ''+ (chartName[17] === 1 ? 'pointer' : 'default') +''
        }}
             className={circleClass}
             onMouseOver={chartName[17] === 1 ? this.props.onhover : this.emptyFunction}
             onMouseOut={chartName[17] === 1 ? this.props.afterhover : this.emptyFunction}
             id="17-G"
             cx="158.62761"
             cy="155.54271"
             r="6.1739259" />
          <circle
          style={{opacity:''+ chartName[18] +'',
          fillOpacity:'1',
          strokeWidth:'.52999997',
          strokeMiterlimit:'4',
          strokeDasharray:'none',
          strokeOpacity:'1',
          cursor: ''+ (chartName[0] === 1 ? 'pointer' : 'default') +''
        }}
             className={circleClass}
             onMouseOver={chartName[18] === 1 ? this.props.onhover : this.emptyFunction}
             onMouseOut={chartName[18] === 1 ? this.props.afterhover : this.emptyFunction}
             id="18-G#"
             cx="57.007862"
             cy="176.97395"
             r="6.1739259" />
          <circle
          style={{opacity:''+ chartName[19] +'',
          fillOpacity:'1',
          strokeWidth:'.52999997',
          strokeMiterlimit:'4',
          strokeDasharray:'none',
          strokeOpacity:'1',
          cursor: ''+ (chartName[19] === 1 ? 'pointer' : 'default') +''
        }}
             className={circleClass}
             onMouseOver={chartName[19] === 1 ? this.props.onhover : this.emptyFunction}
             onMouseOut={chartName[19] === 1 ? this.props.afterhover : this.emptyFunction}
             r="6.1739259"
             cy="176.97395"
             cx="77.400528"
             id="19-C#"/>
          <circle
          style={{opacity:''+ chartName[20] +'',
          fillOpacity:'1',
          strokeWidth:'.52999997',
          strokeMiterlimit:'4',
          strokeDasharray:'none',
          strokeOpacity:'1',
          cursor: ''+ (chartName[20] === 1 ? 'pointer' : 'default') +''
        }}
             className={circleClass}
             onMouseOver={chartName[20] === 1 ? this.props.onhover : this.emptyFunction}
             onMouseOut={chartName[20] === 1 ? this.props.afterhover : this.emptyFunction}
             id="20-F#"
             cx="97.773445"
             cy="176.97395"
             r="6.1739259" />
          <circle
          style={{opacity:''+ chartName[21] +'',
          fillOpacity:'1',
          strokeWidth:'.52999997',
          strokeMiterlimit:'4',
          strokeDasharray:'none',
          strokeOpacity:'1',
          cursor: ''+ (chartName[21] === 1 ? 'pointer' : 'default') +''
        }}
             className={circleClass}
             onMouseOver={chartName[21] === 1 ? this.props.onhover : this.emptyFunction}
             onMouseOut={chartName[21] === 1 ? this.props.afterhover : this.emptyFunction}
             r="6.1739259"
             cy="176.97395"
             cx="118.14637"
             id="21-B"/>
          <circle
          style={{opacity:''+ chartName[22] +'',
          fillOpacity:'1',
          strokeWidth:'.52999997',
          strokeMiterlimit:'4',
          strokeDasharray:'none',
          strokeOpacity:'1',
          cursor: ''+ (chartName[22] === 1 ? 'pointer' : 'default') +''
        }}
             className={circleClass}
             onMouseOver={chartName[22] === 1 ? this.props.onhover : this.emptyFunction}
             onMouseOut={chartName[22] === 1 ? this.props.afterhover : this.emptyFunction}
             id="22-D#"
             cx="138.51927"
             cy="176.97395"
             r="6.1739259" />
          <circle
          style={{opacity:''+ chartName[23] +'',
          fillOpacity:'1',
          strokeWidth:'.52999997',
          strokeMiterlimit:'4',
          strokeDasharray:'none',
          strokeOpacity:'1',
          cursor: ''+ (chartName[23] === 1 ? 'pointer' : 'default') +''
        }}
             className={circleClass}
             onMouseOver={chartName[23] === 1 ? this.props.onhover : this.emptyFunction}
             onMouseOut={chartName[23] === 1 ? this.props.afterhover : this.emptyFunction}
             r="6.1739259"
             cy="176.97395"
             cx="158.62761"
             id="23-G#"/>
          <circle
          style={{opacity:''+ chartName[24] +'',
          fillOpacity:'1',
          strokeWidth:'.52999997',
          strokeMiterlimit:'4',
          strokeDasharray:'none',
          strokeOpacity:'1',
          cursor: ''+ (chartName[24] === 1 ? 'pointer' : 'default') +''
        }}
             className={circleClass}
             onMouseOver={chartName[24] === 1 ? this.props.onhover : this.emptyFunction}
             onMouseOut={chartName[24] === 1 ? this.props.afterhover : this.emptyFunction}
             r="6.1739259"
             cy="197.76645"
             cx="57.007862"
             id="24-A"/>
          <circle
          style={{opacity:''+ chartName[25] +'',
          fillOpacity:'1',
          strokeWidth:'.52999997',
          strokeMiterlimit:'4',
          strokeDasharray:'none',
          strokeOpacity:'1',
          cursor: ''+ (chartName[25] === 1 ? 'pointer' : 'default') +''
        }}
             className={circleClass}
             onMouseOver={chartName[25] === 1 ? this.props.onhover : this.emptyFunction}
             onMouseOut={chartName[25] === 1 ? this.props.afterhover : this.emptyFunction}
             id="25-D"
             cx="77.400528"
             cy="197.76645"
             r="6.1739259" />
          <circle
          style={{opacity:''+ chartName[26] +'',
          fillOpacity:'1',
          strokeWidth:'.52999997',
          strokeMiterlimit:'4',
          strokeDasharray:'none',
          strokeOpacity:'1',
          cursor: ''+ (chartName[26] === 1 ? 'pointer' : 'default') +''
        }}
             className={circleClass}
             onMouseOver={chartName[26] === 1 ? this.props.onhover : this.emptyFunction}
             onMouseOut={chartName[26] === 1 ? this.props.afterhover : this.emptyFunction}
             r="6.1739259"
             cy="197.76645"
             cx="97.773445"
             id="26-G"/>
          <circle
          style={{opacity:''+ chartName[27] +'',
          fillOpacity:'1',
          strokeWidth:'.52999997',
          strokeMiterlimit:'4',
          strokeDasharray:'none',
          strokeOpacity:'1',
          cursor: ''+ (chartName[27] === 1 ? 'pointer' : 'default') +''
        }}
             className={circleClass}
             onMouseOver={chartName[27] === 1 ? this.props.onhover : this.emptyFunction}
             onMouseOut={chartName[27] === 1 ? this.props.afterhover : this.emptyFunction}
             id="27-C"
             cx="118.14637"
             cy="197.76645"
             r="6.1739259" />
          <circle
          style={{opacity:''+ chartName[28] +'',
          fillOpacity:'1',
          strokeWidth:'.52999997',
          strokeMiterlimit:'4',
          strokeDasharray:'none',
          strokeOpacity:'1',
          cursor: ''+ (chartName[28] === 1 ? 'pointer' : 'default') +''
        }}
            className={circleClass}
            onMouseOver={chartName[28] === 1 ? this.props.onhover : this.emptyFunction}
            onMouseOut={chartName[28] === 1 ? this.props.afterhover : this.emptyFunction}
             r="6.1739259"
             cy="197.76645"
             cx="138.51927"
             id="28-E"/>
          <circle
          style={{opacity:''+ chartName[29] +'',
          fillOpacity:'1',
          strokeWidth:'.52999997',
          strokeMiterlimit:'4',
          strokeDasharray:'none',
          strokeOpacity:'1',
          cursor: ''+ (chartName[29] === 1 ? 'pointer' : 'default') +''
        }}
             className={circleClass}
             onMouseOver={chartName[29] === 1 ? this.props.onhover : this.emptyFunction}
             onMouseOut={chartName[29] === 1 ? this.props.afterhover : this.emptyFunction}
             id="29-A"
             cx="158.62761"
             cy="197.76645"
             r="6.1739259" />
        </g>
      </svg>
      </React.Fragment>
    );
  }

}

export default Chart;
