import React, {Component} from 'react';
import {Form, FormGroup, Label, Jumbotron, Input, Button} from 'reactstrap';
import FirstComponent from '../components/ChordCreator/FirstComponent';
import SecondComponent from '../components/ChordCreator/SecondComponent';
import ThirdComponent from '../components/ChordCreator/ThirdComponent';

class ChordCreator extends Component{
  constructor(props){
    super(props);
    this.state = {
      currentPage: 1,
      chordName: '',
      chartName: '',
      fretBoardArray: [0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0], //78 notes on the fretboard
        fretArrays: {
          firstFret: [0, 1, 2, 3, 4, 5],
          secondFret: [6, 7, 8, 9, 10, 11],
          thirdFret: [12, 13, 14, 15, 16, 17],
          fourthFret: [18, 19, 20, 21, 22, 23],
          fifthFret: [24, 25, 26, 27, 28, 29],
          sixthFret: [30, 31, 32, 33, 34, 35],
          seventhFret: [36, 37, 38, 39, 40, 41],
          eighthFret: [42, 43, 44, 45, 46, 47],
          ninthFret: [48, 49, 50, 51, 52, 53],
          tenthFret: [54, 55, 56, 57, 58, 59],
          eleventhFret: [60, 61, 62, 63, 64, 65],
          twelfthFret: [66, 67, 68, 69, 70, 71],
          thirteenthFret: [72, 73, 74, 75, 76, 77]
        },
            filledFirst: [],
            filledSecond:[],
            filledThird:[],
            filledFourth:[],
            filledFifth:[],
            filledSixth:[],
            filledSeventh:[],
            filledEighth:[],
            filledNinth:[],
            filledTenth:[],
            filledEleventh:[],
            filledTwelfth:[],
            filledThirteenth:[],
            currentFilled:{},
        selectedFrets: [],
        selectedNotes: []
    }

    this.nextPage = this.nextPage.bind(this);
    this.checkFrets = this.checkFrets.bind(this);
    this.backPage = this.backPage.bind(this);
    this.checkFretsBody = this.checkFretsBody.bind(this);
    this.chordNameChange = this.chordNameChange.bind(this);
    this.chartNameChange = this.chartNameChange.bind(this);
    this.getNotesWithinDistance = this.getNotesWithinDistance.bind(this);
    this.disableNotes = this.disableNotes.bind(this);
    this.enableNotes = this.enableNotes.bind(this);
    this.resetForm = this.resetForm.bind(this);
    this.handleCircleClick = this.handleCircleClick.bind(this);
    this.checkDistance = this.checkDistance.bind(this);
    var f = this.state.filledFirst;
  }

  nextPage(e){
    e.preventDefault();
    if(this.state.currentPage >= 3)
    {
      this.resetForm();
      console.log(this.state.chordName);
      this.setState({currentPage: 1})
    }
    else{
      this.setState({currentPage: this.state.currentPage + 1})
      console.log(this.state.currentPage);
    }
  }

  chordNameChange(e){
    e.preventDefault();
    this.setState({chordName: e.target.value},
    function(){
      console.log(this.state.chordName);
    });
  }

  chartNameChange(e){
    e.preventDefault();
    this.setState({chartName: e.target.value},
    function(){
      console.log(this.state.chartName);
    });
  }

  backPage(e){
    e.preventDefault();
    this.setState({currentPage: this.state.currentPage - 1});
  }

  resetForm(e){
    this.setState({chordName: '', chartName: ''});
  }

  checkFretsBody(filled, name, fret){
    console.log("Length " + filled.length);
    if(filled.length == 0){
      this.setState({selectedFrets: this.state.selectedFrets.splice(fret)});
      console.log(this.state.selectedFrets);
      //console.log("The "+ name +" array is empty");
    }
    else if(filled.length != 0)
    {
      if(this.state.selectedFrets.includes(fret))
      {
        //console.log("Notes already selected on "+ name +" fret");
      }
      else{
        this.state.selectedFrets.push(fret);
        console.log(this.state.selectedFrets);
        this.checkDistance(this.state.selectedFrets);
      }
    }
  }

  checkFrets(){
      this.setState({filledFirst: this.state.selectedNotes.filter(value => this.state.fretArrays.firstFret.includes(value))}, function(){
        this.checkFretsBody(this.state.filledFirst, 'first', 1);
      });
      this.setState({filledSecond: this.state.selectedNotes.filter(value => this.state.fretArrays.secondFret.includes(value))}, function(){
        this.checkFretsBody(this.state.filledSecond, 'second', 2);
      });
      this.setState({filledThird: this.state.selectedNotes.filter(value => this.state.fretArrays.thirdFret.includes(value))}, function(){
        this.checkFretsBody(this.state.filledThird, 'third', 3);
      });
      this.setState({filledFourth: this.state.selectedNotes.filter(value => this.state.fretArrays.fourthFret.includes(value))}, function(){
        this.checkFretsBody(this.state.filledFourth, 'fourth', 4);
      });
      this.setState({filledFifth: this.state.selectedNotes.filter(value => this.state.fretArrays.fifthFret.includes(value))}, function(){
        this.checkFretsBody(this.state.filledFifth, 'fifth', 5);
      });
      this.setState({filledSixth: this.state.selectedNotes.filter(value => this.state.fretArrays.sixthFret.includes(value))}, function(){
        this.checkFretsBody(this.state.filledSixth, 'sixth', 6);
      });
      this.setState({filledSeventh: this.state.selectedNotes.filter(value => this.state.fretArrays.seventhFret.includes(value))}, function(){
        this.checkFretsBody(this.state.filledSeventh, 'seventh', 7);
      });
      this.setState({filledEighth: this.state.selectedNotes.filter(value => this.state.fretArrays.eighthFret.includes(value))}, function(){
        this.checkFretsBody(this.state.filledEighth, 'eighth', 8);
      });
      this.setState({filledNinth: this.state.selectedNotes.filter(value => this.state.fretArrays.ninthFret.includes(value))}, function(){
        this.checkFretsBody(this.state.filledNinth, 'ninth', 9);
      });
      this.setState({filledTenth: this.state.selectedNotes.filter(value => this.state.fretArrays.tenthFret.includes(value))}, function(){
        this.checkFretsBody(this.state.filledTenth, 'tenth', 10);
      });
      this.setState({filledEleventh: this.state.selectedNotes.filter(value => this.state.fretArrays.eleventhFret.includes(value))}, function(){
        this.checkFretsBody(this.state.filledEleventh, 'eleventh', 11);
      });
      this.setState({filledTwelfth: this.state.selectedNotes.filter(value => this.state.fretArrays.twelfthFret.includes(value))}, function(){
        this.checkFretsBody(this.state.filledTwelfth, 'twelfth', 12);
      });
      this.setState({filledThirteenth: this.state.selectedNotes.filter(value => this.state.fretArrays.thirteenthFret.includes(value))}, function(){
        this.checkFretsBody(this.state.filledThirteenth, 'thirteenth', 13);
      });
  }

  checkDistance(values){
    //finds lowest value
    var low = Math.min(...values);
    var high = Math.max(...values);
    var difference = high - low;
    console.log(low);
    console.log(high);
    if(difference >= 3)
    {
      this.getNotesWithinDistance(high, low);
      console.log("This chord is not a playable chord");
    }
  }

  getNotesWithinDistance(high, low){
    console.log("LOW " + low);
    console.log("HIGH " + high);
  }

  disableNotes(){

  }

  enableNotes(){

  }

  handleCircleClick(event){
    if(this.state.fretBoardArray[event.target.id] == 1)
    {
      this.state.fretBoardArray[event.target.id] = 0;
      var array = [...this.state.selectedNotes];
      var index = array.indexOf(event.target.id);
      array.splice(index, 1);
      this.setState({selectedNotes: array}, function(){
        this.checkFrets();
      });
      this.forceUpdate();
    }
    else{
      this.state.selectedNotes.push(parseInt(event.target.id));
      this.state.fretBoardArray[event.target.id] = 1;
      this.checkFrets();
      this.forceUpdate();
    }
  }

  render(){
    switch(this.state.currentPage){
      case 1:
        return (<FirstComponent nextPage={this.nextPage} chordNameChange={this.chordNameChange} chartNameChange={this.chartNameChange} />);
      case 2:
        return (<SecondComponent nextPage={this.nextPage} backPage={this.backPage} fretboard={this.state.fretBoardArray} handlecircleclick={this.handleCircleClick}/>);
      case 3:
        return (<ThirdComponent nextPage={this.nextPage}  backPage={this.backPage} chord={this.state.chordName} chart={this.state.chartName}/>);
    }
  }
}

export default ChordCreator;
