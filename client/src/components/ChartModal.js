import React, {Component} from 'react';
import Chart from '../components/Chart';
import {Button, Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';

class ChartModal extends Component{
  constructor(props){
    super(props);
    this.state = {
      modal: false
    };
    this.toggle = this.toggle.bind(this);
  }

  toggle(){
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }

  render(){
    var chordChartStyle = {
      background: 'transparent',
      width: 'auto'
    }
    var modalBodyStyle = {
      background: 'white',
      color:'black'
    }
    var modalContent;
    var modalFooterContent;
    if(this.props.label == 'view'){
      modalContent =
        <React.Fragment>
        <div className="modalChartContainer">
        <h2>This is the content for view</h2>
        <br/>
          <div className="modalChartBox">
          <div style={chordChartStyle}>
            <Chart chartclass={this.props.chartclass} circleclass={this.props.circleclass} chartName={this.props.chartName}/>
          </div>
          </div>
        </div>
        </React.Fragment>;
    }
    else{
      modalContent =
      <React.Fragment>
      <h2>This is the content for the save button</h2>
      <form>
        <select>
          <option value="JazzChords">Jazz Chords</option>
          <option value="BluesChords">Blues Chords</option>
          <option value="SadSoundingChords">Sad Sounding Chords</option>
        </select></form>
      </React.Fragment>
      modalFooterContent = <Button color="primary" onClick={this.toggle}>Save Chord</Button>;
    }

    return(
      <React.Fragment>
        <Button style={this.props.btnStyle} color={this.props.buttonStyle} onClick={this.toggle}>{this.props.label}</Button>
        <Modal size="lg" isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle}></ModalHeader>
          <ModalBody  style={modalBodyStyle}>
            {modalContent}
          </ModalBody>
          <ModalFooter>
            {modalFooterContent}
            <Button color="secondary" onClick={this.toggle}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </React.Fragment>
    );
  }
}

export default ChartModal;
