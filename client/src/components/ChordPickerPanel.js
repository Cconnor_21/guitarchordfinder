import React, {Component} from 'react';
import {
  Jumbotron,
   Button,
   Dropdown,
   DropdownToggle,
   DropdownMenu,
   DropdownItem,
   Form,
   FormGroup,
   Input,
   Label,
   InputGroup
 } from 'reactstrap';

class Panel extends Component{
  constructor(props){
    super(props);
    this.state = {
    };

    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false
    };
  }

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  render(){
    var jumbotronStyle = {
      marginBottom: '0',
      borderRadius: '0px',
      padding: '20px'
    };

    var dropDownStyle = {
      margin: '6px'
    }
    return (
      <div style={{}}>
            <Jumbotron className="vertical-center" style={jumbotronStyle}>
              <h1 className="display-3"><b>{this.props.noteValue}</b>{this.props.keyValue}</h1>
              <p className="lead">Use the dropdowns to find different chords to play</p>
              <div className="inputGroup" style={{display: 'flex', alignItems:'center', justifyContent: 'center'}}>
              <div style={{width: '400px'}}>
              <Form onSubmit={this.props.submit}>
                <FormGroup>
          <InputGroup size="normal">
          <Input type="select" name="select" id="exampleSelect" placeholder="sm" bsSize="sm" style={dropDownStyle} onChange={this.props.noteChange}>
          <option value="" disabled selected>Choose a note</option>
          <option value="A">A</option>
          <option value="A♭/B#">A♭/B#</option>
          <option value="B">B</option>
          <option value="B♭/C#">B♭/C#</option>
          <option value="C">C</option>
          <option value="C♭/D#">C♭/D#</option>
          <option value="D">D</option>
          <option value="D♭/E#">D♭/E#</option>
          <option value="E">E</option>
          <option value="E♭/F#">E♭/F#</option>
          <option value="F">F</option>
          <option value="F♭/G#">F♭/G#</option>
          <option value="G">G</option>
          </Input>
          <Input type="select" name="select" id="exampleSelect2" placeholder="sm" bsSize="sm" style={dropDownStyle} onChange={this.props.keyChange}>
          <option value="" disabled selected>Choose a key</option>
          <option value="maj">maj</option>
          <option value="m">m</option>
          <option value="sus">sus</option>
          </Input>
          </InputGroup>
                </FormGroup>
                <Button color="primary">Find Chords</Button>
              </Form>
              </div>
              </div>
            </Jumbotron>
          </div>
    );
  }
}

export default Panel;
