import React, {Component} from 'react';
import {
  Jumbotron,
   Button,
   Dropdown,
   DropdownToggle,
   DropdownMenu,
   DropdownItem,
   Form,
   FormGroup,
   Input,
   Label,
   InputGroup
 } from 'reactstrap';

class ChartPickerPanel extends Component{
  constructor(props){
    super(props);
    this.state = {
    };

    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false
    };
  }

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  render(){
    var jumbotronStyle = {
      marginBottom: '0',
      borderRadius: '0px',
      padding: '20px'
    };

    var dropDownStyle = {
      margin: '6px'
    }
    return(

      <div style={{}}>
            <Jumbotron className="vertical-center" style={jumbotronStyle}>
              <h3 style={{fontSize: '45px'}} className="display-3"><b>{this.props.chordChart}</b></h3>
              <p className="lead">View your saved chord charts</p>
              <div className="inputGroup" style={{display: 'flex', alignItems:'center', justifyContent: 'center'}}>
              <div style={{width: '400px'}}>
              <Form onSubmit={this.props.submit}>
                <FormGroup>
          <InputGroup size="normal">
          <Input type="select" name="select" id="exampleSelect" placeholder="sm" bsSize="sm" style={dropDownStyle} onChange={this.props.chartChange}>
          <option value="" disabled selected>Select chord chart</option>
          <option value="Blues Chord Chart">Blues Chord Chart</option>
          <option value="Jazz Chord Chart">Jazz Chord Chart</option>
          <option value="Sad Sounding Chords Chart">Sad Sounding Chords Chart</option>
          </Input>
          </InputGroup>
                </FormGroup>
                <Button color="primary">View Chart</Button>
              </Form>
              </div>
              </div>
            </Jumbotron>
          </div>
    );
  }
}

export default ChartPickerPanel;
