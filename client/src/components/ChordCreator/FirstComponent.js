import React, {Component} from 'react';
import {Button, Jumbotron, Form, FormGroup, Label, Input} from 'reactstrap';

class FirstComponent extends Component{
  render(){
    return(
      <React.Fragment>
        <Jumbotron>
        <Form>
          <FormGroup>
            <Input onChange={this.props.chordNameChange} name="name" id="chordName" placeholder="Enter the name of your chord" />
          </FormGroup>
          <FormGroup>
          <Label for="exampleSelect">Choose a chord chart</Label>
          <Input onChange={this.props.chartNameChange} type="select" name="select" id="exampleSelect">
            <option>Blues Chords</option>
            <option>Jazz Chords</option>
            <option>Sad sounding guitar chords</option>
          </Input>
        </FormGroup>
        </Form>
        <Button onClick={this.props.nextPage}>Next</Button>
        </Jumbotron>
      </React.Fragment>
    );
  }
}

export default FirstComponent;
