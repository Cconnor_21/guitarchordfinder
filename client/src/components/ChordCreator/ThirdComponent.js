import React, {Component} from 'react';
import {Button, Jumbotron} from 'reactstrap';

class ThirdComponent extends Component{
  render(){
    return(
      <React.Fragment>
        <Jumbotron>
        <div>You have created the chord <b>{this.props.chord}</b> and saved to the chart <b>{this.props.chart}</b></div>
        <Button onClick={this.props.backPage}>Back</Button>
        <Button onClick={this.props.nextPage}>Finish</Button>
        </Jumbotron>
      </React.Fragment>
    );
  }
}

export default ThirdComponent;
