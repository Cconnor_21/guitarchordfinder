import React, {Component} from 'react';
import {Button, Jumbotron} from 'reactstrap';
import FullFretboard from '../FullFretboard';

class SecondComponent extends Component{
  constructor(props){
    super(props);
      this.state={
      }
  }

  render(){
    return(
      <React.Fragment>
        <Jumbotron>
        <div>Select notes between four frets to create a chord</div>
        <FullFretboard fretCircle={'fretCircle'} fretboard={this.props.fretboard} handlecircleclick={this.props.handlecircleclick}/>
        <Button onClick={this.props.backPage}>Back</Button>
        <Button onClick={this.props.nextPage}>Next</Button>
        </Jumbotron>
      </React.Fragment>
    );
  }
}

export default SecondComponent;
