import React, {Component} from 'react';
import '../App.css';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Jumbotron,
  Button} from 'reactstrap';
  import {Link, NavLink as RouterNavLink} from "react-router-dom";

class MainNavbar extends Component{
  constructor(props){
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };

  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render(){
    return(
      <Navbar color="primary" light expand="md">
          <NavbarBrand href="/">GuitarChordCharts</NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav style={{marginRight: '10px' }} className="ml-auto" navbar>
              <NavItem>
                <NavLink to="/" activeClassName="active" tag={RouterNavLink}>Find Guitar Chords</NavLink>
              </NavItem>
              <NavItem>
                <NavLink to="/chartpicker" tag={RouterNavLink}>Guitar Charts</NavLink>
              </NavItem>
              <NavItem>
                <NavLink to="/chordcreator" tag={RouterNavLink}>Chord Creator</NavLink>
              </NavItem>
            </Nav>
            <Button style={{marginRight: '10px' }} color="secondary">Register</Button>
            <Button color="secondary">Log In</Button>
          </Collapse>
        </Navbar>
    );
  }
}

export default MainNavbar;
