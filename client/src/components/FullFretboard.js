import React, {Component} from 'react';

class FullFretboard extends Component{
  constructor(props){
    super(props);
    this.state={

    }
  }
  render(){
    var fretBoard = this.props.fretboard;

    var circleStyling = {
      opacity:'1',
      fill:'#000000',
      fillOpacity:'1',
      stroke:'#000000',
      strokeWidth:'1.66499972',
      strokeLinecap:'round',
      strokeMiterlimit:'4',
      strokeDasharray:'none',
      strokeOpacity:'1',
      cursor:'pointer'
    }

    var pathStyling = {
      color:'#000000',
      fontStyle:'normal',
      fontVariant:'normal',
      fontWeight:'normal',
      fontStretch:'normal',
      fontSize:'medium',
      lineHeight:'normal',
      fontFamily:'sans-serif',
      fontVariantLigatures:'normal',
      fontVariantPosition:'normal',
      fontVariantCaps:'normal',
      fontVariantNumeric:'normal',
      fontVariantAlternates:'normal',
      fontFeatureSettings:'normal',
      textIndent:'0',
      textAlign:'start',
      textDecoration:'none',
      textDecorationLine:'none',
      textDecorationStyle:'solid',
      textDecorationColor:'#000000',
      letterSpacing:'normal',
      wordSpacing:'normal',
      textTransform:'none',
      writingMode:'lr-tb',
      direction:'ltr',
      textOrientation:'mixed',
      dominantBaseline:'auto',
      baselineShift:'baseline',
      textAnchor:'start',
      whiteSpace:'normal',
      shapePadding:'0',
      clipRule:'nonzero',
      display:'inline',
      overflow:'visible',
      visibility:'visible',
      isolation:'auto',
      mixBlendMode:'normal',
      colorInterpolation:'sRGB',
      colorInterpolationFilters:'linearRGB',
      solidColor:'#000000',
      solidOpacity:'1',
      vectorEffect:'none',
      fill:'#000000',
      fillOpacity:'1',
      fillRule:'nonzero',
      stroke:'none',
      strokeWidth:'6.29291344',
      strokeLinecap:'butt',
      strokeLinejoin:'miter',
      strokeMiterlimit:'4',
      strokeDasharray:'none',
      strokeDashoffset:'0',
      strokeOpacity:'1',
      colorRendering:'auto',
      imageRendering:'auto',
      shapeRendering:'auto',
      textRendering:'auto',
      enableBackground:'accumulate'
    }
    var fretCircle = this.props.fretCircle;
    return(
      <div>
      <svg
         version="1.1"
         viewBox="0 0 394.06271 123.35113"
         height="70mm">
        <defs
           id="defs2" />
        <metadata
           id="metadata5">
        </metadata>
        <g
           transform="translate(-235.65507,-78.673608)"
           id="layer1">
          <g
             transform="translate(-188.70464,-10.574111)"
             id="layer1-7">
            <path
               className={pathStyling}
               id="path886"
               transform="matrix(0.26458333,0,0,0.26458333,188.70464,10.574111)"
               d="m 947.00195,333.15234 v 0.0215 h -29.625 v 390.86914 h 29.625 v 0.0176 H 2347.6641 v -0.0156 h 0.3535 V 333.18164 h -0.3535 v -0.0293 z m 18.66797,6.29688 h 90.59958 v 71.15625 h -90.59958 z m 96.88868,0 h 100.623 v 71.15625 h -100.623 z m 106.9121,0 h 100.6211 v 71.15625 h -100.6211 z m 106.9121,0 h 100.6211 v 71.15625 h -100.6211 z m 106.9121,0 h 100.6133 v 71.15625 h -100.6133 z m 106.9102,0 h 100.6152 v 71.15625 h -100.6152 z m 106.9121,0 h 100.6152 v 71.15625 h -100.6152 z m 106.9121,0 h 100.6152 v 71.15625 h -100.6152 z m 106.9121,0 h 100.6152 v 71.15625 h -100.6152 z m 106.9043,0 h 100.6231 v 71.15625 h -100.6231 z m 106.9121,0 h 100.6231 v 71.15625 h -100.6231 z m 106.9121,0 h 100.6231 v 71.15625 h -100.6231 z m 106.9121,0 h 103.1465 v 71.15625 H 2238.582 Z M 965.66992,416.90234 h 90.59958 v 70.27539 h -90.59958 z m 96.88868,0 h 100.623 v 70.27539 h -100.623 z m 106.9121,0 h 100.6211 v 70.27539 h -100.6211 z m 106.9121,0 h 100.6211 v 70.27539 h -100.6211 z m 106.9121,0 h 100.6133 v 70.27539 h -100.6133 z m 106.9102,0 h 100.6152 v 70.27539 h -100.6152 z m 106.9121,0 h 100.6152 v 70.27539 h -100.6152 z m 106.9121,0 h 100.6152 v 70.27539 h -100.6152 z m 106.9121,0 h 100.6152 v 70.27539 h -100.6152 z m 106.9043,0 h 100.6231 v 70.27539 h -100.6231 z m 106.9121,0 h 100.6231 v 70.27539 h -100.6231 z m 106.9121,0 h 100.6231 v 70.27539 h -100.6231 z m 106.9121,0 h 103.1465 v 70.27539 H 2238.582 Z M 965.66992,493.4668 h 90.59958 v 70.27539 h -90.59958 z m 96.88868,0 h 100.623 v 70.27539 h -100.623 z m 106.9121,0 h 100.6211 v 70.27539 h -100.6211 z m 106.9121,0 h 100.6211 v 70.27539 h -100.6211 z m 106.9121,0 h 100.6133 v 70.27539 h -100.6133 z m 106.9102,0 h 100.6152 v 70.27539 h -100.6152 z m 106.9121,0 h 100.6152 v 70.27539 h -100.6152 z m 106.9121,0 h 100.6152 v 70.27539 h -100.6152 z m 106.9121,0 h 100.6152 v 70.27539 h -100.6152 z m 106.9043,0 h 100.6231 v 70.27539 h -100.6231 z m 106.9121,0 h 100.6231 v 70.27539 h -100.6231 z m 106.9121,0 h 100.6231 v 70.27539 h -100.6231 z m 106.9121,0 h 103.1465 v 70.27539 H 2238.582 Z M 965.66992,570.03906 h 90.59958 v 70.27539 h -90.59958 z m 96.88868,0 h 100.623 v 70.27539 h -100.623 z m 106.9121,0 h 100.6211 v 70.27539 h -100.6211 z m 106.9121,0 h 100.6211 v 70.27539 h -100.6211 z m 106.9121,0 h 100.6133 v 70.27539 h -100.6133 z m 106.9102,0 h 100.6152 v 70.27539 h -100.6152 z m 106.9121,0 h 100.6152 v 70.27539 h -100.6152 z m 106.9121,0 h 100.6152 v 70.27539 h -100.6152 z m 106.9121,0 h 100.6152 v 70.27539 h -100.6152 z m 106.9043,0 h 100.6231 v 70.27539 h -100.6231 z m 106.9121,0 h 100.6231 v 70.27539 h -100.6231 z m 106.9121,0 h 100.6231 v 70.27539 h -100.6231 z m 106.9121,0 h 103.1465 v 70.27539 H 2238.582 Z M 965.66992,646.61133 h 90.59958 v 71.16015 h -90.59958 z m 96.88868,0 h 100.623 v 71.16015 h -100.623 z m 106.9121,0 h 100.6211 v 71.16015 h -100.6211 z m 106.9121,0 h 100.6211 v 71.16015 h -100.6211 z m 106.9121,0 h 100.6133 v 71.16015 h -100.6133 z m 106.9102,0 h 100.6152 v 71.16015 h -100.6152 z m 106.9121,0 h 100.6152 v 71.16015 h -100.6152 z m 106.9121,0 h 100.6152 v 71.16015 h -100.6152 z m 106.9121,0 h 100.6152 v 71.16015 h -100.6152 z m 106.9043,0 h 100.6231 v 71.16015 h -100.6231 z m 106.9121,0 h 100.6231 v 71.16015 h -100.6231 z m 106.9121,0 h 100.6231 v 71.16015 h -100.6231 z m 106.9121,0 h 103.1465 v 71.16015 H 2238.582 Z"/>
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={circleStyling, {opacity: '' + fretBoard[0] + ''}}
               r="5.8700948"
               cy="100.22127"
               cx="456.12628"
               id="0"
               />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[1] + ''}}
               id="1"
               cx="456.12628"
               cy="120.25401"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[2] + ''}}
               r="5.8700948"
               cy="140.66473"
               cx="456.12628"
               id="2"/>
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[3] + ''}}
               id="3"
               cx="456.12628"
               cy="160.69748"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[4] + ''}}
               r="5.8700948"
               cy="181.1082"
               cx="456.12628"
               id="4"/>
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[5] + ''}}
               id="5"
               cx="456.78775"
               cy="201.51892"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[6] + ''}}
               id="6"
               cx="483.34058"
               cy="100.22127"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[7] + ''}}
               r="5.8700948"
               cy="120.25401"
               cx="483.34058"
               id="7"/>
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[8] + ''}}
               id="8"
               cx="483.34058"
               cy="140.66473"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[9] + ''}}
               r="5.8700948"
               cy="160.69748"
               cx="483.34058"
               id="9"
               />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[10] + ''}}
               id="10"
               cx="483.34058"
               cy="181.1082"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[11] + ''}}
               r="5.8700948"
               cy="201.51892"
               cx="484.00204"
               id="11"
               />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[12] + ''}}
               r="5.8700948"
               cy="100.22127"
               cx="512.06677"
               id="12"
               />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[13] + ''}}
               id="13"
               cx="512.06677"
               cy="120.25401"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[14] + ''}}
               r="5.8700948"
               cy="140.66473"
               cx="512.06677"
               id="14"
               />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[15] + ''}}
               id="15"
               cx="512.06677"
               cy="160.69748"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[16] + ''}}
               r="5.8700948"
               cy="181.1082"
               cx="512.06677"
               id="16"
               />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[17] + ''}}
               id="17"
               cx="512.72821"
               cy="201.51892"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[18] + ''}}
               id="18"
               cx="540.41498"
               cy="100.22127"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[19] + ''}}
               r="5.8700948"
               cy="120.25401"
               cx="540.41498"
               id="19"
               />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[20] + ''}}
               id="20"
               cx="540.41498"
               cy="140.66473"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[21] + ''}}
               r="5.8700948"
               cy="160.69748"
               cx="540.41498"
               id="21"
               />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[22] + ''}}
               id="22"
               cx="540.41498"
               cy="181.1082"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[23] + ''}}
               r="5.8700948"
               cy="201.51892"
               cx="541.07642"
               id="23"
               />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[24] + ''}}
               r="5.8700948"
               cy="100.22127"
               cx="568.38519"
               id="24"
               />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[25] + ''}}
               id="25"
               cx="568.38519"
               cy="120.25401"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[26] + ''}}
               r="5.8700948"
               cy="140.66473"
               cx="568.38519"
               id="26"
              />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[27] + ''}}
               id="27"
               cx="568.38519"
               cy="160.69748"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[28] + ''}}
               r="5.8700948"
               cy="181.1082"
               cx="568.38519"
               id="28"
               />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[29] + ''}}
               id="29"
               cx="569.04663"
               cy="201.51892"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[30] + ''}}
               id="30"
               cx="596.7334"
               cy="100.22127"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[31] + ''}}
               r="5.8700948"
               cy="120.25401"
               cx="596.7334"
               id="31"
               />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[32] + ''}}
               id="32"
               cx="596.7334"
               cy="140.66473"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[33] + ''}}
               r="5.8700948"
               cy="160.69748"
               cx="596.7334"
               id="33"/>
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[34] + ''}}
               id="34"
               cx="596.7334"
               cy="181.1082"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[35] + ''}}
               r="5.8700948"
               cy="201.51892"
               cx="597.39484"
               id="35"
               />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[36] + ''}}
               r="5.8700948"
               cy="100.22127"
               cx="625.45959"
               id="36"/>
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[37] + ''}}
               id="37"
               cx="625.45959"
               cy="120.25401"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[38] + ''}}
               r="5.8700948"
               cy="140.66473"
               cx="625.45959"
               id="38"/>
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[39] + ''}}
               id="39"
               cx="625.45959"
               cy="160.69748"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[40] + ''}}
               r="5.8700948"
               cy="181.1082"
               cx="625.45959"
               id="40" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[41] + ''}}
               id="41"
               cx="626.12103"
               cy="201.51892"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[42] + ''}}
               id="42"
               cx="653.42981"
               cy="100.22127"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[43] + ''}}
               r="5.8700948"
               cy="120.25401"
               cx="653.42981"
               id="43"/>
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[44] + ''}}
               id="44"
               cx="653.42981"
               cy="140.66473"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[45] + ''}}
               r="5.8700948"
               cy="160.69748"
               cx="653.42981"
               id="45"/>
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[46] + ''}}
               id="46"
               cx="653.42981"
               cy="181.1082"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[47] + ''}}
               r="5.8700948"
               cy="201.51892"
               cx="654.09125"
               id="47"/>
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[48] + ''}}
               r="5.8700948"
               cy="100.22127"
               cx="681.40002"
               id="48"/>
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[49] + ''}}
               id="49"
               cx="681.40002"
               cy="120.25401"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[50] + ''}}
               r="5.8700948"
               cy="140.66473"
               cx="681.40002"
               id="50"/>
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[51] + ''}}
               id="51"
               cx="681.40002"
               cy="160.69748"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[52] + ''}}
               r="5.8700948"
               cy="181.1082"
               cx="681.40002"
               id="52"/>
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[53] + ''}}
               id="53"
               cx="682.06146"
               cy="201.51892"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[54] + ''}}
               id="54"
               cx="710.26514"
               cy="100.22127"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[55] + ''}}
               r="5.8700948"
               cy="120.25401"
               cx="710.26514"
               id="55"/>
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[56] + ''}}
               id="56"
               cx="710.26514"
               cy="140.66473"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[57] + ''}}
               r="5.8700948"
               cy="160.69748"
               cx="710.26514"
               id="57"/>
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[58] + ''}}
               id="58"
               cx="710.26514"
               cy="181.1082"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[59] + ''}}
               r="5.8700948"
               cy="201.51892"
               cx="710.92657"
               id="59"
               />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[60] + ''}}
               r="5.8700948"
               cy="100.22127"
               cx="738.06116"
               id="60"
               />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[61] + ''}}
               id="61"
               cx="738.06116"
               cy="120.25401"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[62] + ''}}
               r="5.8700948"
               cy="140.66473"
               cx="738.06116"
               id="62"
               />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[63] + ''}}
               id="63"
               cx="738.06116"
               cy="160.69748"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[64] + ''}}
               r="5.8700948"
               cy="181.1082"
               cx="738.06116"
               id="64"
               />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[65] + ''}}
               id="65"
               cx="738.7226"
               cy="201.51892"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[66] + ''}}
               id="66"
               cx="765.85718"
               cy="100.22127"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[67] + ''}}
               r="5.8700948"
               cy="120.25401"
               cx="765.85718"
               id="67"
               />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[68] + ''}}
               id="68"
               cx="765.85718"
               cy="140.66473"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[69] + ''}}
               r="5.8700948"
               cy="160.69748"
               cx="765.85718"
               id="69"
               />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[70] + ''}}
               id="70"
               cx="765.85718"
               cy="181.1082"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[71] + ''}}
               r="5.8700948"
               cy="201.51892"
               cx="766.51862"
               id="71"
               />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[72] + ''}}
               r="5.8700948"
               cy="100.22127"
               cx="794.72229"
               id="72"
               />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[73] + ''}}
               id="73"
               cx="794.72229"
               cy="120.25401"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[74] + ''}}
               r="5.8700948"
               cy="140.66473"
               cx="794.72229"
               id="74"
               />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[75] + ''}}
               id="75"
               cx="794.72229"
               cy="160.69748"
               r="5.8700948" />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[76] + ''}}
               r="5.8700948"
               cy="181.1082"
               cx="794.72229"
               id="76"
               />
            <circle
               className={fretCircle}
               onClick={this.props.handlecircleclick}
               style={{opacity: '' + fretBoard[77] + ''}}
               id="77"
               cx="795.38373"
               cy="201.51892"
               r="5.8700948" />
          </g>
        </g>
      </svg>
      </div>
    );
  }
}

export default FullFretboard;
