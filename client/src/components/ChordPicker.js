import React, {Component} from 'react';
import ChordPickerPanel from '../components/ChordPickerPanel';
import Charts from '../components/Charts';

class ChordPicker extends Component{
  constructor(props){
    super(props);
    this.state = {
      noteValue: 'A',
      keyValue: 'm',
      chosenChord: 'Am',
    };
    this.handleNoteChange = this.handleNoteChange.bind(this);
    this.handleKeyChange = this.handleKeyChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleNoteChange(event){
    this.setState({noteValue: event.target.value});
  }

  handleKeyChange(event){
    this.setState({keyValue: event.target.value});
  }

  handleSubmit(event){
    event.preventDefault();
    this.setState({chosenChord: this.state.noteValue + this.state.keyValue});
  }
  render(){
    return(
      <React.Fragment>
      <ChordPickerPanel noteValue={this.state.noteValue} keyValue={this.state.keyValue} noteChange={this.handleNoteChange} keyChange={this.handleKeyChange} submit={this.handleSubmit}/>
      <Charts chosenChord={this.state.chosenChord} />
      </React.Fragment>
    );
  }
}

export default ChordPicker;
