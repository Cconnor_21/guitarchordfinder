import React, {Component} from 'react';
import MainNavbar from './components/MainNavbar';
import {BrowserRouter as Router, Route, Link} from "react-router-dom";
import ChordPicker from './components/ChordPicker';
import ChartPicker from './components/ChartPicker';
import ChordCreator from './components/ChordCreator';
import './output.css';
import './App.css';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {};
  }

  render(){
    return (
      <Router>
        <MainNavbar />
        <Route path="/" exact component={ChordPicker} />
        <Route path="/chartpicker" component={ChartPicker} />
        <Route path="/chordcreator" component={ChordCreator}/>
      </Router>
    );
  }
}

export default App;
